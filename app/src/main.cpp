// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QtQuick>
#include <auroraapp.h>

#include "settings.h"
#include "eventlogger.h"
#include "pensettingscontroller.h"
#include "createemptyimagedialogcontroller.h"
#include "imageeditordialogcontroller.h"
#include "cropselectioncalculator.h"

static const auto IMAGE_OPTION = QStringLiteral("image");

QScopedPointer<EventLogger> logger;

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    logger->handleMessage(type, context, msg);
}

QString extractImageFromArguments(QScopedPointer<QGuiApplication> &application)
{
    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addOptions({ { { "i", IMAGE_OPTION },
                          QCoreApplication::translate("main", "Image to show in the editor"),
                          QCoreApplication::translate("main", "image") }

    });
    parser.process(*application);
    QString imagePath = parser.value(IMAGE_OPTION);
    return imagePath;
}

int main(int argc, char *argv[])
{
    logger.reset(new EventLogger());
    qInstallMessageHandler(messageHandler);
    QScopedPointer<QGuiApplication> application(Aurora::Application::application(argc, argv));
    application->setOrganizationName(QStringLiteral("ru.auroraos"));
    application->setApplicationName(QStringLiteral("ImageEditor"));

    qmlRegisterType<Settings>("ru.auroraos.ImageEditor", 1, 0, "Settings");
    qmlRegisterType<PenSettingsController>("ru.auroraos.ImageEditor", 1, 0,
                                           "PenSettingsController");
    qmlRegisterType<CreateEmptyImageDialogController>("ru.auroraos.ImageEditor", 1, 0,
                                                      "CreateEmptyImageDialogController");
    qmlRegisterType<ImageEditorDialogController>("ru.auroraos.ImageEditor", 1, 0,
                                                 "ImageEditorDialogController");
    qmlRegisterType<CropSelectionCalculator>("ru.auroraos.ImageEditor", 1, 0,
                                             "CropSelectionCalculator");

    QString imagePath = extractImageFromArguments(application);
    QScopedPointer<QQuickView> view(Aurora::Application::createView());
    view->rootContext()->setContextProperty("globalImagePath", imagePath);
    view->setSource(Aurora::Application::pathTo(QStringLiteral("qml/ImageEditor.qml")));
    view->show();

    return application->exec();
}

// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PENSETTINGSCONTROLLER_H
#define PENSETTINGSCONTROLLER_H

#include <QObject>
#include <QColor>
#include <QVariant>
#include <QVariantMap>

#include "settings.h"

class PenSettingsController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(Settings *settings READ settings WRITE setSettings NOTIFY settingsChanged)
    Q_PROPERTY(
            QColor currentColor READ currentColor WRITE setCurrentColor NOTIFY currentColorChanged)
    Q_PROPERTY(int penSize READ penSize WRITE setPenSize NOTIFY penSizeChanged)
    Q_PROPERTY(QVariantList colors READ colors NOTIFY colorsChanged)
public:
    explicit PenSettingsController(QObject *parent = nullptr);

    // Getters
    Settings *settings();
    QVariantList colors() const;
    QColor currentColor() const;
    int penSize();

    // Setters
    void setSettings(Settings *settings);
    void setCurrentColor(const QColor color);
    void setPenSize(int size);

    Q_INVOKABLE void submitPenSizeAndColor();

private:
    Settings *m_settings;
    QVariantList m_colors;
    QColor m_currentColor;
    int m_penSize;

    void updateColors(const QColor color);

signals:
    void settingsChanged();
    void colorsChanged();
    void currentColorChanged();
    void penSizeChanged();
};

#endif // PENSETTINGSCONTROLLER_H

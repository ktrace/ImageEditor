// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QDateTime>
#include <QPixmap>
#include <QPainter>

#include "createemptyimagedialogcontroller.h"

/*!
 * \brief Default constructor.
 * \param parent QObject parent instance.
 */
CreateEmptyImageDialogController::CreateEmptyImageDialogController(QObject *parent)
    : QObject(parent)
{
}

/*!
 * \brief Creates and saves new image with the given parameters.
 * \param width Image width.
 * \param height Image height.
 * \param imageFormat Image format: bmp, jpg or png.
 * \param backgroundColor Image background color.
 * \param quality Image quality (compression setting) for jpg and png formats.
 * \param saveToPath Path to save an image.
 * \return Created image full path.
 */
QString CreateEmptyImageDialogController::createAndSaveImage(const int width, const int height,
                                                             const QString &imageFormat,
                                                             const QColor &backgroundColor,
                                                             const int quality, QString saveToPath)
{
    QString dateMark(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    QString imageName(QString("IMG_%1.%2").arg(dateMark).arg(imageFormat));
    QString imageFullPath = saveToPath.append("/").append(imageName);
    QPixmap resultPixmap(width, height);
    resultPixmap.fill(backgroundColor);
    resultPixmap.save(imageFullPath, imageFormat.toUtf8().data(),
                      imageFormat.compare("bmp") ? -1 : quality);
    return imageFullPath;
}

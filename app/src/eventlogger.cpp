// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "eventlogger.h"

#include <QJsonDocument>
#include <QFileInfo>

/*!
 * \brief The constructor
 * Sets output directory, prepares output stream
 * \param dir a directory to store files
 */
EventLogger::EventLogger(const QString dir) : m_showLogs(true), m_logFileDate(QDate::currentDate())
{
    directory = dir + "/logs/";
    cleanup();
    prepareOutputStream();
}

/*!
 * \brief The destructor
 * Closes output file
 */
EventLogger::~EventLogger()
{
    debugFile.close();
}

/*!
 * Handles outgoing console message:
 * console.log() as Debug
 * console.error() as Critical
 * console.debug() as Debug
 * console.exception() as Critical
 * console.info() as Info
 * console.warn() as Warning
 * \param msgType Type of the outgoing message
 * \param context Context at which message happened
 * \param msg Content of the outgoing message
 */
void EventLogger::handleMessage(QtMsgType msgType, const QMessageLogContext &context,
                                const QString &msg)
{
    if (!m_showLogs)
        return;
    QString message = QString("%1: [%2] - %3 in (%4, %5:%6)")
                              .arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"),
                                   messageTypes.value(msgType), msg, context.file, context.function,
                                   QString::number(context.line));
    changeLogFileIfNeeded();
    if (msgType == QtDebugMsg) {
#ifdef QT_DEBUG
        debugStream << message << endl;
#endif
    } else {
        debugStream << message << endl;
    }
#ifdef QT_DEBUG
    fputs(message.append("\n").toUtf8(), stderr);
#endif
}

/*!
 * \brief Converts the given QVariantMap object to string in JSON format.
 * \param map QVariantMap to convert to JSON-string.
 * \return String with the given data in JSON format.
 */
QString EventLogger::variantMapToJsonString(const QVariantMap &map)
{
    return QString(QJsonDocument::fromVariant(map).toJson(QJsonDocument::Compact));
}

void EventLogger::setShowLogs(const bool showLogs)
{
    m_showLogs = showLogs;
}

/*!
 * \brief Deletes journal files older than 7 days
 */
void EventLogger::cleanup()
{
    qInfo() << "Clear log files older than 7 days.";
    QFileInfoList list = QDir(directory).entryInfoList(QStringList() << "*.txt");
    foreach (const QFileInfo &file, list) {
        if (file.created().addDays(7) < QDateTime::currentDateTime()) {
            QFile::remove(file.absoluteFilePath());
        }
    }
}

/*!
 * \brief Creates required directory and file, starts stream into file
 */
void EventLogger::prepareOutputStream()
{
    if (prepareWritableFile()) {
        qInfo() << QString("Лог-файл %1 открыт на запись.").arg(debugFile.fileName());
        debugStream.setDevice(&debugFile);
        debugStream.setCodec(QTextCodec::codecForName("utf-8"));
    } else {
        qFatal("Ошибка открытия лог-файла %s.", debugFile.fileName().toUtf8().data());
    }
}

/*!
 * \brief Creates a writable directory and a file to store output
 * \return true if file was created and can be written
 */
bool EventLogger::prepareWritableFile()
{
    fileName = m_logFileDate.toString(Qt::ISODate) + ".txt";
    if (!QDir().exists(directory)) {
        QDir().mkpath(directory);
    }
    debugFile.setFileName(directory + fileName);
    qInfo() << QString("Попытка открытия лог-файла %1.").arg(debugFile.fileName());
    return debugFile.open(QIODevice::WriteOnly | QIODevice::Append);
}

/*!
 * \brief Changes the log file to file with current date in its name if a new day has begun
 */
void EventLogger::changeLogFileIfNeeded()
{
    QDate date = QDate::currentDate();
    if (m_logFileDate != date) { // If a new day has begun
        qInfo() << QString("Начались новые сутки.");
        debugFile.close();
        m_logFileDate = date;
        cleanup();
        prepareOutputStream();
    }
}

// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QFile>
#include "imageeditordialogcontroller.h"

const double ImageEditorDialogController::s_defaultBrightness = 0.0;
const double ImageEditorDialogController::s_defaultContrast = 0.0;

ImageEditorDialogController::ImageEditorDialogController(QObject *parent) : QObject(parent),
    m_brightness(s_defaultBrightness),
    m_contrast(s_defaultContrast),
    m_rotation(0),
    m_brightnessEdited(false),
    m_contrastEdited(false),
    m_rotationEdited(false)
{
}

Settings *ImageEditorDialogController::settings()
{
    return m_settings;
}

double ImageEditorDialogController::brightness() const
{
    return m_brightness;
}

double ImageEditorDialogController::contrast() const
{
    return m_contrast;
}

int ImageEditorDialogController::rotation() const
{
    return m_rotation;
}

bool ImageEditorDialogController::brightnessEdited() const
{
    return m_brightnessEdited;
}

bool ImageEditorDialogController::contrastEdited() const
{
    return m_contrastEdited;
}

bool ImageEditorDialogController::rotationEdited() const
{
    return m_rotationEdited;
}

void ImageEditorDialogController::setSettings(Settings *settings)
{
    m_settings = settings;
    emit settingsChanged();
}

void ImageEditorDialogController::setBrightness(const double brightness)
{
    m_brightness = brightness;
    if (!m_brightnessEdited)
        setBrightnessEdited(true);
    emit brightnessChanged();
}

void ImageEditorDialogController::setContrast(const double contrast)
{
    m_contrast = contrast;
    if (!m_contrastEdited)
        setContrastEdited(true);
    emit contrastChanged();
}

void ImageEditorDialogController::addRotation(const int rotation)
{
    m_rotation += rotation;
    if (!m_rotationEdited)
        setRotationEdited(true);
    emit rotationChanged();
}

void ImageEditorDialogController::resetBrightness()
{
    setBrightness(s_defaultBrightness);
    setBrightnessEdited(false);
}

void ImageEditorDialogController::resetContrast()
{
    setContrast(s_defaultContrast);
    setContrastEdited(false);
}

double ImageEditorDialogController::defaultBrightness() const
{
    return s_defaultBrightness;
}

double ImageEditorDialogController::defaultContrast() const
{
    return s_defaultContrast;
}

void ImageEditorDialogController::deleteFile(const QString &filePath)
{
    QFile::remove(filePath);
}

void ImageEditorDialogController::setBrightnessEdited(const bool brightnessEdited)
{
    m_brightnessEdited = brightnessEdited;
    emit brightnessEditedChanged();
}

void ImageEditorDialogController::setContrastEdited(const bool contrastEdited)
{
    m_contrastEdited = contrastEdited;
    emit contrastEditedChanged();
}

void ImageEditorDialogController::setRotationEdited(const bool rotationEdited)
{
    m_rotationEdited = rotationEdited;
    emit rotationEditedChanged();
}

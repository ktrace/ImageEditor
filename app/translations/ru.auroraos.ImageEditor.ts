<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="23"/>
        <source>About Application</source>
        <translation>About Application</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="32"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;Full featured image editor for Aurora OS&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="43"/>
        <source>3-Clause BSD License</source>
        <translation>3-Clause BSD License</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="52"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;i&gt;Copyright (c) 2023. Open Mobile Platform LLC.&lt;/i&gt;&lt;/p&gt;
&lt;p&gt;Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:&lt;/p&gt;
&lt;ol&gt;
	&lt;li&gt;Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.&lt;/li&gt;
	&lt;li&gt;Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.&lt;/li&gt;
	&lt;li&gt;Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.&lt;/li&gt;
&lt;/ol&gt;
&lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>CreateEmptyImageDialog</name>
    <message>
        <location filename="../qml/dialogs/CreateEmptyImageDialog.qml" line="49"/>
        <source>Create an image</source>
        <translation>Create an image</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/CreateEmptyImageDialog.qml" line="50"/>
        <source>Create</source>
        <translation>Create</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/CreateEmptyImageDialog.qml" line="51"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/CreateEmptyImageDialog.qml" line="64"/>
        <source>Width, min: 200, max: %1</source>
        <translation>Width, min: 200, max: %1</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/CreateEmptyImageDialog.qml" line="65"/>
        <source>Width</source>
        <translation>Width</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/CreateEmptyImageDialog.qml" line="81"/>
        <source>Height, min: 200, max: %1</source>
        <translation>Height, min: 200, max: %1</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/CreateEmptyImageDialog.qml" line="82"/>
        <source>Height</source>
        <translation>Height</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/CreateEmptyImageDialog.qml" line="96"/>
        <source>Image format</source>
        <translation>Image format</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/CreateEmptyImageDialog.qml" line="118"/>
        <source>Quality</source>
        <translation>Quality</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/CreateEmptyImageDialog.qml" line="131"/>
        <source>Background color</source>
        <translation>Background color</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/CreateEmptyImageDialog.qml" line="132"/>
        <source>Select</source>
        <translation>Select</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/CreateEmptyImageDialog.qml" line="132"/>
        <source>Transparent</source>
        <translation>Transparent</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/CreateEmptyImageDialog.qml" line="160"/>
        <source>Save to</source>
        <translation>Save to</translation>
    </message>
</context>
<context>
    <name>CropFormatPage</name>
    <message>
        <location filename="../qml/pages/CropFormatPage.qml" line="22"/>
        <source>Cropping</source>
        <translation>Cropping</translation>
    </message>
    <message>
        <location filename="../qml/pages/CropFormatPage.qml" line="39"/>
        <source>Original</source>
        <translation>Original</translation>
    </message>
    <message>
        <location filename="../qml/pages/CropFormatPage.qml" line="42"/>
        <source>Custom</source>
        <translation>Custom</translation>
    </message>
    <message>
        <location filename="../qml/pages/CropFormatPage.qml" line="45"/>
        <source>Square</source>
        <translation>Square</translation>
    </message>
</context>
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="12"/>
        <source>Image Editor</source>
        <translation>Image Editor</translation>
    </message>
</context>
<context>
    <name>ImageEditorDialog</name>
    <message>
        <location filename="../qml/dialogs/ImageEditorDialog.qml" line="199"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/ImageEditorDialog.qml" line="200"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/ImageEditorDialog.qml" line="294"/>
        <source>Pen settings</source>
        <translation>Pen settings</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/ImageEditorDialog.qml" line="327"/>
        <source>Brightness</source>
        <translation>Brightness</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/ImageEditorDialog.qml" line="354"/>
        <source>Contrast</source>
        <translation>Contrast</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="25"/>
        <source>Image Editor</source>
        <translation>Image Editor</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="56"/>
        <source>Create new image</source>
        <translation>Create new image</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="63"/>
        <source>Open image in editor</source>
        <translation>Open image in editor</translation>
    </message>
</context>
<context>
    <name>PenSettingsPage</name>
    <message>
        <location filename="../qml/pages/PenSettingsPage.qml" line="49"/>
        <source>Pen size</source>
        <translation>Pen size</translation>
    </message>
    <message>
        <location filename="../qml/pages/PenSettingsPage.qml" line="97"/>
        <source>Pen color</source>
        <translation>Pen color</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/settings.cpp" line="85"/>
        <source>Original</source>
        <translation>Original</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="87"/>
        <source>Custom</source>
        <translation>Custom</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="89"/>
        <source>Square</source>
        <translation>Square</translation>
    </message>
</context>
<context>
    <name>SaveImageDialog</name>
    <message>
        <location filename="../qml/dialogs/SaveImageDialog.qml" line="37"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/SaveImageDialog.qml" line="38"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/SaveImageDialog.qml" line="42"/>
        <source>Do you want to save new image?</source>
        <translation>Do you want to save new image?</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/SaveImageDialog.qml" line="65"/>
        <source>Saving method:</source>
        <translation>Saving method:</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/SaveImageDialog.qml" line="88"/>
        <source>Replace</source>
        <translation>Replace</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/SaveImageDialog.qml" line="89"/>
        <source>Replace source file with edited image</source>
        <translation>Replace source file with edited image</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/SaveImageDialog.qml" line="109"/>
        <source>Copy</source>
        <translation>Copy</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/SaveImageDialog.qml" line="110"/>
        <source>Create edited copy of a source file</source>
        <translation>Create edited copy of a source file</translation>
    </message>
</context>
<context>
    <name>SelectFolderDialog</name>
    <message>
        <location filename="../qml/dialogs/SelectFolderDialog.qml" line="62"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="28"/>
        <source>Image to show in the editor</source>
        <translation>Image to show in the editor</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="29"/>
        <source>image</source>
        <translation>image</translation>
    </message>
</context>
</TS>

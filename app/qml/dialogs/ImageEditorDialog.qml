// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import Sailfish.Silica 1.0
import ru.auroraos.ImageEditor 1.0
import "../components"

Dialog {
    id: imageEditorDialog

    property string imagePath
    property bool shouldCreateNewImage: false
    property string folderPathForNewFile: ""

    acceptDestination: Qt.resolvedUrl("SaveImageDialog.qml")
    acceptDestinationAction: PageStackAction.Push

    onDone: {
        if (result === DialogResult.Accepted) {
            imageEditorArea.requestImage(function (result) {
                    if (acceptDestination) {
                        acceptDestinationInstance.imagePath = imagePath;
                        acceptDestinationInstance.image = result;
                        acceptDestinationInstance.shouldCreateNewImage = shouldCreateNewImage;
                        acceptDestinationInstance.folderPathForNewFile = folderPathForNewFile;
                    }
                });
        }
    }

    onRejected: {
        if (shouldCreateNewImage)
            controller.deleteFile(imagePath);
        pageStack.replaceAbove(null, Qt.resolvedUrl("../pages/MainPage.qml"));
    }

    onStateChanged: imageEditorArea.tool = state

    objectName: "imageEditorDialog"
    canAccept: shouldCreateNewImage || imageEditorArea.paintCanUndo || controller.brightnessEdited || controller.contrastEdited || imageEditorArea.cropCanUndo || controller.rotationEdited
    backNavigation: !imageEditorArea.active
    forwardNavigation: !imageEditorArea.active

    state: "PEN_ACTIVE"
    states: [
        State {
            name: "INACTIVE"
            PropertyChanges {
                target: penToggleButton
                toggled: false
            }
            PropertyChanges {
                target: brightnessToggleButton
                toggled: false
            }
            PropertyChanges {
                target: contrastToggleButton
                toggled: false
            }
            PropertyChanges {
                target: cropToggleButton
                toggled: false
            }
            PropertyChanges {
                target: rotateToggleButton
                toggled: false
            }
        },
        State {
            name: "PEN_ACTIVE"
            PropertyChanges {
                target: penToggleButton
                toggled: true
            }
            PropertyChanges {
                target: brightnessToggleButton
                toggled: false
            }
            PropertyChanges {
                target: contrastToggleButton
                toggled: false
            }
            PropertyChanges {
                target: cropToggleButton
                toggled: false
            }
            PropertyChanges {
                target: rotateToggleButton
                toggled: false
            }
        },
        State {
            name: "BRIGHTNESS_ACTIVE"
            PropertyChanges {
                target: penToggleButton
                toggled: false
            }
            PropertyChanges {
                target: brightnessToggleButton
                toggled: true
            }
            PropertyChanges {
                target: contrastToggleButton
                toggled: false
            }
            PropertyChanges {
                target: cropToggleButton
                toggled: false
            }
            PropertyChanges {
                target: rotateToggleButton
                toggled: false
            }
        },
        State {
            name: "CONTRAST_ACTIVE"
            PropertyChanges {
                target: penToggleButton
                toggled: false
            }
            PropertyChanges {
                target: brightnessToggleButton
                toggled: false
            }
            PropertyChanges {
                target: contrastToggleButton
                toggled: true
            }
            PropertyChanges {
                target: cropToggleButton
                toggled: false
            }
            PropertyChanges {
                target: rotateToggleButton
                toggled: false
            }
        },
        State {
            name: "CROPPING_ACTIVE"
            PropertyChanges {
                target: penToggleButton
                toggled: false
            }
            PropertyChanges {
                target: brightnessToggleButton
                toggled: false
            }
            PropertyChanges {
                target: contrastToggleButton
                toggled: false
            }
            PropertyChanges {
                target: cropToggleButton
                toggled: true
            }
            PropertyChanges {
                target: rotateToggleButton
                toggled: false
            }
        },
        State {
            name: "ROTATE_ACTIVE"
            PropertyChanges {
                target: penToggleButton
                toggled: false
            }
            PropertyChanges {
                target: brightnessToggleButton
                toggled: false
            }
            PropertyChanges {
                target: contrastToggleButton
                toggled: false
            }
            PropertyChanges {
                target: cropToggleButton
                toggled: false
            }
            PropertyChanges {
                target: rotateToggleButton
                toggled: true
            }
        }
    ]

    ImageEditorDialogController {
        id: controller

        onRotationChanged: {
            imageEditorArea.transform(Qt.point(imageEditorArea.width / 2, imageEditorArea.height / 2), rotation);
        }
    }

    DialogHeader {
        id: dialogHeader

        objectName: "dialogHeader"
        acceptText: qsTr("Save")
        cancelText: qsTr("Cancel")
    }

    ImageEditorArea {
        id: imageEditorArea

        objectName: "imageEditorArea"
        image.source: imagePath
        anchors {
            left: parent.left
            right: parent.right
            top: dialogHeader.bottom
            bottom: toolOverlay.top
        }
        clip: true
        penColor: appSettings.penColor
        penSize: appSettings.penSize
        brightness: controller.brightness
        contrast: controller.contrast
        cropFormat: appSettings.cropFormat
        leftPadding: Theme.itemSizeMedium / 2
        rightPadding: Theme.itemSizeMedium / 2
        topPadding: Theme.itemSizeMedium / 2
        bottomPadding: Theme.itemSizeMedium / 2
    }

    Item {
        id: toolOverlay

        objectName: "toolOverlay"
        height: contentColumn.height + Theme.paddingMedium * 2
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        Rectangle {
            anchors.fill: parent
            color: Theme.overlayBackgroundColor
            opacity: 0.45
        }

        Column {
            id: contentColumn

            objectName: "contentColumn"
            spacing: Theme.paddingMedium

            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
                bottomMargin: Theme.paddingMedium
                topMargin: Theme.paddingMedium
            }

            Item {
                id: currentToolControl

                height: Theme.itemSizeLarge
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }

                BackgroundItem {
                    id: penControl

                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    visible: penToggleButton.toggled

                    Row {
                        id: penControlRow

                        anchors.centerIn: parent
                        spacing: Theme.paddingLarge

                        Rectangle {
                            id: penSample

                            anchors.verticalCenter: parent.verticalCenter
                            height: appSettings.penSize
                            width: height
                            radius: height / 2
                            color: appSettings.penColor
                        }

                        Text {
                            text: qsTr("Pen settings")
                            color: Theme.primaryColor
                            font.pixelSize: Theme.fontSizeLarge
                            anchors.verticalCenter: penSample.verticalCenter

                            MouseArea {
                                onClicked: pageStack.push(Qt.resolvedUrl("../pages/PenSettingsPage.qml"), {
                                        "controller": penSettingsController
                                    })

                                anchors.fill: parent
                            }
                        }
                    }

                    ResetSettingButton {
                        anchors {
                            right: parent.right
                            verticalCenter: parent.verticalCenter
                        }
                        visible: imageEditorArea.paintCanUndo
                        onClicked: imageEditorArea.undoPaint()
                    }
                }

                Slider {
                    id: brightnessSlider

                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    visible: brightnessToggleButton.toggled
                    label: qsTr("Brightness")
                    minimumValue: -1
                    maximumValue: 1
                    stepSize: 0.005
                    onSliderValueChanged: controller.brightness = sliderValue
                    Component.onCompleted: controller.resetBrightness()

                    ResetSettingButton {
                        id: brightnessResetButton

                        anchors.right: parent.right
                        visible: controller.brightnessEdited
                        onClicked: {
                            brightnessSlider.value = controller.defaultBrightness();
                            controller.resetBrightness();
                        }
                    }
                }

                Slider {
                    id: contrastSlider

                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    visible: contrastToggleButton.toggled
                    label: qsTr("Contrast")
                    minimumValue: -1
                    maximumValue: 1
                    stepSize: 0.005
                    onSliderValueChanged: controller.contrast = sliderValue
                    Component.onCompleted: controller.resetContrast()

                    ResetSettingButton {
                        anchors.right: parent.right
                        visible: controller.contrastEdited
                        onClicked: {
                            contrastSlider.value = controller.defaultContrast();
                            controller.resetContrast();
                        }
                    }
                }

                BackgroundItem {
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    visible: cropToggleButton.toggled

                    Label {
                        id: cropControl

                        anchors.centerIn: parent
                        text: appSettings.cropFormatName
                        font.pixelSize: Theme.fontSizeLarge

                        MouseArea {
                            onPressed: cropControl.color = Theme.highlightColor
                            onReleased: cropControl.color = Theme.primaryColor
                            onClicked: pageStack.push(Qt.resolvedUrl("../pages/CropFormatPage.qml"))
                            anchors.fill: parent
                        }
                    }

                    ResetSettingButton {
                        anchors {
                            right: parent.right
                            verticalCenter: parent.verticalCenter
                        }
                        visible: imageEditorArea.cropCanUndo
                        onClicked: imageEditorArea.undoCrop()
                    }
                }

                Row {
                    id: rotateControl

                    spacing: Theme.paddingLarge
                    height: childrenRect.height
                    width: childrenRect.width
                    anchors.centerIn: parent
                    visible: rotateToggleButton.toggled

                    IconButton {
                        id: rightRotateButton

                        icon.source: "image://theme/icon-m-rotate-right?" + (pressed ? Theme.highlightColor : Theme.primaryColor)
                        onClicked: controller.addRotation(90)
                    }

                    IconButton {
                        id: leftRotateButton

                        icon.source: "image://theme/icon-m-rotate-left?" + (pressed ? Theme.highlightColor : Theme.primaryColor)
                        onClicked: controller.addRotation(-90)
                    }
                }
            }

            Row {
                spacing: Theme.paddingLarge

                anchors.horizontalCenter: parent.horizontalCenter

                ToggleButton {
                    id: penToggleButton

                    icon.source: "image://theme/icon-m-edit?" + (toggled ? Theme.highlightColor : Theme.primaryColor)
                    onClicked: setupState("PEN_ACTIVE")
                }

                ToggleButton {
                    id: brightnessToggleButton

                    icon.source: Theme.colorScheme === Theme.DarkOnLight ? Qt.resolvedUrl("../icons/dark/icon-m-brightness.svg") : Qt.resolvedUrl("../icons/light/icon-m-brightness.svg")
                    highlighted: toggled
                    onClicked: setupState("BRIGHTNESS_ACTIVE")
                }

                ToggleButton {
                    id: contrastToggleButton

                    icon.source: Theme.colorScheme === Theme.DarkOnLight ? Qt.resolvedUrl("../icons/dark/icon-m-contrast.svg") : Qt.resolvedUrl("../icons/light/icon-m-contrast.svg")
                    highlighted: toggled
                    onClicked: setupState("CONTRAST_ACTIVE")
                }

                ToggleButton {
                    id: cropToggleButton

                    icon.source: "image://theme/icon-m-crop?" + (toggled ? Theme.highlightColor : Theme.primaryColor)
                    onClicked: setupState("CROPPING_ACTIVE")
                }

                ToggleButton {
                    id: rotateToggleButton

                    icon.source: "image://theme/icon-m-refresh?" + (toggled ? Theme.highlightColor : Theme.primaryColor)
                    onClicked: setupState("ROTATE_ACTIVE")
                }
            }

            PenSettingsController {
                id: penSettingsController

                settings: appSettings
            }
        }
    }

    function enableNavigation(enable) {
        dialogHeader.visible = enable;
        imageEditorDialog.forwardNavigation = enable;
        imageEditorDialog.backNavigation = enable;
    }

    function setupState(state) {
        if (imageEditorDialog.state === state)
            imageEditorDialog.state = "INACTIVE";
        else
            imageEditorDialog.state = state;
    }
}

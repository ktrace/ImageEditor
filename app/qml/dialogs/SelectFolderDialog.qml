// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import Qt.labs.folderlistmodel 2.1

Dialog {

    property string currentFolder: StandardPaths.pictures
    property string returnFolder: currentFolder.replace("file://", "").split("/").length > 3 ? currentFolder.substring(0, currentFolder.lastIndexOf("/")) : ""

    FolderListModel {
        id: folderModel

        folder: currentFolder
        showFiles: false
    }

    SilicaFlickable {
        anchors.fill: parent

        DialogHeader {
            id: dialogHeader

            title: returnFolder.length > 0 ? currentFolder.replace("file://", "").replace(StandardPaths.home + "/", "") : ""
        }

        BackgroundItem {
            id: returnButton

            anchors {
                top: dialogHeader.bottom
                left: parent.left
                right: parent.right
                bottomMargin: Theme.paddingMedium
            }
            contentHeight: Theme.itemSizeMedium
            highlighted: pressed
            visible: returnFolder.length > 0

            Icon {
                id: returnIcon

                anchors {
                    left: parent.left
                    verticalCenter: parent.verticalCenter
                    leftMargin: Theme.horizontalPageMargin
                }
                source: "image://theme/icon-m-back"
            }
            Label {
                id: label

                anchors {
                    left: returnIcon.right
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                    leftMargin: Theme.paddingMedium
                    rightMargin: Theme.horizontalPageMargin
                }
                text: qsTr("Back")
            }

            onClicked: currentFolder = returnFolder
        }

        SilicaListView {
            anchors {
                left: parent.left
                right: parent.right
                top: returnButton.visible ? returnButton.bottom : dialogHeader.bottom
                bottom: parent.bottom
            }
            model: folderModel
            delegate: ListItem {
                id: listItem

                width: parent.width
                contentHeight: Theme.itemSizeMedium
                highlighted: pressed

                Icon {
                    id: itemIcon

                    anchors {
                        left: parent.left
                        verticalCenter: parent.verticalCenter
                        leftMargin: Theme.horizontalPageMargin
                    }
                    sourceSize {
                        height: Theme.itemSizeSmall
                        width: Theme.itemSizeSmall
                    }
                    source: folderModel.isFolder(index) ? "image://theme/icon-m-folder" : "image://theme/icon-m-file-audio"
                }

                Label {
                    anchors {
                        left: itemIcon.right
                        right: parent.right
                        verticalCenter: parent.verticalCenter
                        rightMargin: Theme.horizontalPageMargin
                        leftMargin: Theme.paddingMedium
                    }
                    text: model.fileName
                }

                onClicked: currentFolder = folderModel.folder + "/" + model.fileName
            }

            VerticalScrollDecorator {
            }
        }
    }
}

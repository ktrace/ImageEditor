// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.5
import Sailfish.Silica 1.0

Item {
    id: root

    objectName: "imageEditorArea"

    readonly property alias image: sourceImage
    readonly property alias imageRotation: targetImage.rotation

    property alias brightness: targetImage.brightness
    property alias contrast: targetImage.contrast
    property alias paintCanUndo: viewportPainterArea.canUndo
    property alias cropCanUndo: cropArea.canUndo
    property alias cropFormat: cropArea.cropFormat
    property alias penColor: painterArea.lineColor
    property alias penSize: painterArea.lineWidth

    property string tool: "PEN_ACTIVE"
    property real leftPadding: 0
    property real rightPadding: 0
    property real topPadding: 0
    property real bottomPadding: 0
    property int prevRotation: 0
    property real minScale
    property real maxScale: 4.0

    readonly property bool active: painterArea.painting || cropArea.cropInProgress

    function requestImage(callback) {
        viewport.grabToImage(callback);
    }

    function undoPaint() {
        viewportPainterArea.undo();
    }

    function undoCrop() {
        viewport.width = targetImage.width;
        viewport.height = targetImage.height;
        viewport.scale = Math.min((cropArea.width - leftPadding - rightPadding) / viewport.width, (cropArea.height - topPadding - bottomPadding) / viewport.height);
        minScale = viewport.scale;
        cropArea.viewfinder.width = viewport.width * viewport.scale;
        cropArea.viewfinder.height = viewport.height * viewport.scale;
        cropArea.viewfinder.x = (cropArea.width - cropArea.viewfinder.width) / 2;
        cropArea.viewfinder.y = (cropArea.height - cropArea.viewfinder.height) / 2;
        viewport.update(false);
        targetImage.x = 0;
        targetImage.y = 0;
        cropArea.canUndo = false;
    }

    function reset() {
        if (sourceImage.status !== Image.Ready)
            return;
        undoCrop();
        targetImage.rotation = 0;
    }

    function transform(origin, rotation, scale) {
        viewport.transformContent({
                "imageRotation": rotation,
                "viewportScale": scale
            }, {
                "origin": mapToItem(viewport, origin.x, origin.y)
            });
    }

    onWidthChanged: viewport.align()
    onHeightChanged: viewport.align()

    Item {
        id: viewport

        objectName: "viewport"

        function update(steadyContent) {
            if (steadyContent) {
                targetImage.x -= (cropArea.viewfinder.x - x) / scale;
                targetImage.y -= (cropArea.viewfinder.y - y) / scale;
            }
            x = cropArea.viewfinder.x;
            y = cropArea.viewfinder.y;
            width = cropArea.viewfinder.width / scale;
            height = cropArea.viewfinder.height / scale;
        }

        function align() {
            var scaleFactor = Math.min((cropArea.width - leftPadding - rightPadding) / cropArea.viewfinder.width, (cropArea.height - topPadding - bottomPadding) / cropArea.viewfinder.height);
            scale *= scaleFactor;
            updateCropAreaViewfinder();
            update(false);
        }

        function alignContent() {
            updateCropAreaViewfinder();
            update(true);
        }

        function updateCropAreaViewfinder() {
            cropArea.viewfinder.width = width * scale;
            cropArea.viewfinder.height = height * scale;
            cropArea.viewfinder.x = (cropArea.width - cropArea.viewfinder.width) / 2;
            cropArea.viewfinder.y = (cropArea.height - cropArea.viewfinder.height) / 2;
        }

        function rotateViewport(targetImageRotation, scaledStartViewportSize, targetViewportScale) {
            if (prevRotation === targetImageRotation) {
                viewport.width = scaledStartViewportSize.width / targetViewportScale;
                viewport.height = scaledStartViewportSize.height / targetViewportScale;
            } else {
                viewport.width = scaledStartViewportSize.height / targetViewportScale;
                viewport.height = scaledStartViewportSize.width / targetViewportScale;
                prevRotation = targetImageRotation;
            }
        }

        function transformContent(target, start) {
            var startImageRotation = start.imageRotation === undefined ? targetImage.rotation : start.imageRotation;
            var targetImageRotation = target.imageRotation === undefined ? startImageRotation : target.imageRotation;
            var startViewportScale = start.viewportScale === undefined ? viewport.scale : start.viewportScale;
            var targetViewportScale = target.viewportScale === undefined ? startViewportScale : target.viewportScale;
            var scaledStartOrigin = start.origin === undefined ? Qt.point(targetImage.x * startViewportScale, targetImage.y * startViewportScale) : Qt.point(start.origin.x * startViewportScale, start.origin.y * startViewportScale);
            var scaledTargetOrigin = target.origin === undefined ? scaledStartOrigin : Qt.point(target.origin.x * viewport.scale, target.origin.y * viewport.scale);
            var scaledStartImagePosition = start.imagePosition === undefined ? Qt.point(targetImage.x * startViewportScale, targetImage.y * startViewportScale) : Qt.point(start.imagePosition.x * startViewportScale, start.imagePosition.y * startViewportScale);
            var scaledStartViewportSize = start.viewportSize === undefined ? Qt.size(viewport.width * startViewportScale, viewport.height * startViewportScale) : Qt.size(start.viewportSize.width * startViewportScale, start.viewportSize.height * startViewportScale);
            var scaledStartOffset = Qt.point(scaledStartImagePosition.x - scaledStartOrigin.x, scaledStartImagePosition.y - scaledStartOrigin.y);
            var rotationDelta = (targetImageRotation - startImageRotation) * Math.PI / 180;
            var rotationDeltaCos = Math.cos(rotationDelta);
            var rotationDeltaSin = Math.sin(rotationDelta);
            var scaledRotatedOffset = Qt.point((scaledStartOffset.x * rotationDeltaCos - scaledStartOffset.y * rotationDeltaSin), (scaledStartOffset.x * rotationDeltaSin + scaledStartOffset.y * rotationDeltaCos));
            if (tool !== "ROTATE_ACTIVE") {
                targetImage.x = scaledTargetOrigin.x / targetViewportScale + scaledRotatedOffset.x / startViewportScale;
                targetImage.y = scaledTargetOrigin.y / targetViewportScale + scaledRotatedOffset.y / startViewportScale;
            }
            targetImage.rotation = targetImageRotation;
            rotateViewport(targetImageRotation, scaledStartViewportSize, targetViewportScale);
            viewport.scale = targetViewportScale;
            viewport.alignContent();
        }

        function boundTargetImage() {
            var divx = targetImage.height / 2 - targetImage.width / 2;
            var divy = targetImage.width / 2 - targetImage.height / 2;
            if ((targetImage.rotation / 90) % 2 === 0) { // horizontal image orientation
                if (targetImage.x > 0) {
                    targetImage.x = 0;
                } else if (targetImage.x * viewport.scale + targetImage.width * viewport.scale < cropArea.viewfinder.width) {
                    targetImage.x = (cropArea.viewfinder.width - targetImage.width * viewport.scale) / viewport.scale;
                }
                if (targetImage.y > 0) {
                    targetImage.y = 0;
                } else if (targetImage.y * viewport.scale + targetImage.height * viewport.scale < cropArea.viewfinder.height) {
                    targetImage.y = (cropArea.viewfinder.height - targetImage.height * viewport.scale) / viewport.scale;
                }
            } else { // vertical image orientation
                if (targetImage.x > divx) {
                    targetImage.x = divx
                } else if ((targetImage.x - divx) * viewport.scale + targetImage.height * viewport.scale < cropArea.viewfinder.width) {
                    targetImage.x = (cropArea.viewfinder.width - targetImage.height * viewport.scale) / viewport.scale + divx;
                }
                if (targetImage.y > divy) {
                    targetImage.y = divy
                } else if ((targetImage.y - divy) * viewport.scale  + targetImage.width * viewport.scale < cropArea.viewfinder.height) {
                    targetImage.y = (cropArea.viewfinder.height - targetImage.width * viewport.scale) / viewport.scale + divy;
                }
            }
        }

        transformOrigin: Item.TopLeft
        clip: root.tool !== "CROPPING_ACTIVE"

        Rectangle {
            anchors.fill: parent
            color: "black"
        }

        ColoredItem {
            id: targetImage

            objectName: "coloredImage"
            transformOrigin: Item.Center

            source: Image {
                id: sourceImage

                objectName: "image"
                asynchronous: true
                autoTransform: true

                onStatusChanged: {
                    if (status === Image.Ready) {
                        targetImage.width = sourceSize.width;
                        targetImage.height = sourceSize.height;
                        root.reset();
                    }
                }
            }

            Behavior on rotation  {
                RotationAnimation {
                    duration: 200
                    onRunningChanged: {
                        if (!running)
                            viewport.boundTargetImage();
                    }
                }
            }

            PainterArea {
                id: viewportPainterArea

                objectName: "viewportPainterArea"
                anchors.fill: parent
                enabled: false
            }
        }
    }

    PinchArea {
        id: pinchArea

        property var startParameters
        property bool active: false

        anchors.fill: parent
        visible: root.tool === "CROPPING_ACTIVE"
        pinch.dragAxis: Pinch.XAndYAxis

        onPinchStarted: {
            startParameters = {
                "origin": mapToItem(viewport, pinch.center.x, pinch.center.y),
                "imageRotation": targetImage.rotation,
                "imagePosition": Qt.point(targetImage.x, targetImage.y),
                "viewportScale": viewport.scale,
                "viewportSize": Qt.size(viewport.width, viewport.height)
            };
            active = true;
            cropArea.canUndo = true;
        }

        onPinchUpdated: {
            var scl = startParameters.viewportScale * pinch.scale;
            if (scl > maxScale)
                scl = maxScale;
            if (scl < minScale)
                scl = minScale;
            viewport.transformContent({
                    "origin": mapToItem(viewport, pinch.center.x, pinch.center.y),
                    "imageRotation": startParameters.imageRotation,
                    "viewportScale": scl
                }, startParameters);
        }

        onPinchFinished: {
            viewport.boundTargetImage();
            active = false;
        }

        CropArea {
            id: cropArea

            objectName: "cropArea"
            anchors.fill: parent
            enabled: !pinchArea.active
            visible: parent.visible
            targetImage: targetImage
            viewportScale: viewport.scale
            viewfinder.border {
                width: 5
                color: Theme.highlightColor
            }
            shadow {
                color: Theme.overlayBackgroundColor
                opacity: 0.45
            }
            interactiveBorderWidth: Theme.itemSizeMedium
            onCropFormatChanged: undoCrop()
            onCropUpdated: viewport.update(true)
            onCropFinished: viewport.align()
        }
    }

    PainterArea {
        id: painterArea

        objectName: "painterArea"
        x: cropArea.viewfinder.x
        y: cropArea.viewfinder.y
        width: cropArea.viewfinder.width
        height: cropArea.viewfinder.height
        visible: root.tool === "PEN_ACTIVE"

        onReleased: moveGraphics(viewportPainterArea, 1 / viewport.scale)
    }

    Component.onCompleted: reset()
}

// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import QtGraphicalEffects 1.0

BrightnessContrast {
    id: root

    objectName: "coloredItem"

    Binding {
        target: root.source
        property: "visible"
        value: false
    }

    Binding {
        target: root.source
        property: "width"
        value: root.width
    }

    Binding {
        target: root.source
        property: "height"
        value: root.height
    }
}

// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: root

    SilicaListView {
        spacing: 1.5 * Theme.paddingLarge
        height: formatModel.count * (Theme.fontSizeLarge + 1.5 * Theme.paddingLarge) + Theme.itemSizeExtraLarge

        anchors {
            verticalCenter: parent.verticalCenter
            left: parent.left
            right: parent.right
        }

        header: Text {
            height: Theme.itemSizeExtraLarge
            text: qsTr("Cropping")
            font.bold: true
            font.pixelSize: Theme.fontSizeLarge
            color: Theme.highlightColor
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            anchors {
                left: parent.left
                right: parent.right
            }
        }

        model: ListModel {
            id: formatModel

            ListElement {
                format: qsTr("Original")
            }
            ListElement {
                format: qsTr("Custom")
            }
            ListElement {
                format: qsTr("Square")
            }
            ListElement {
                format: "4:3"
            }
            ListElement {
                format: "3:4"
            }
        }

        delegate: Label {
            text: format
            font.pixelSize: Theme.fontSizeLarge
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter

            anchors {
                left: parent.left
                right: parent.right
            }
            color: appSettings.cropFormat === index ? Theme.highlightColor : Theme.primaryColor

            MouseArea {
                onClicked: {
                    appSettings.cropFormat = index;
                    pageStack.pop();
                }

                anchors.fill: parent
            }
        }
    }

    IconButton {
        id: closeButton

        onClicked: pageStack.pop()

        icon.source: "image://theme/icon-m-cancel?" + (pressed ? Theme.highlightColor : Theme.primaryColor)

        anchors {
            bottom: parent.bottom
            bottomMargin: Theme.paddingLarge
            horizontalCenter: parent.horizontalCenter
        }
    }
}

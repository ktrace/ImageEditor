// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Pickers 1.0
import Sailfish.Silica 1.0

Page {
    objectName: "mainPage"
    onStatusChanged: {
        if (status == PageStatus.Active) {
            if (globalImagePath) {
                pageStack.completeAnimation();
                pageStack.push(Qt.resolvedUrl("../dialogs/ImageEditorDialog.qml"), {
                        "imagePath": globalImagePath
                    });
            }
        }
    }

    PageHeader {
        id: pageHeader

        objectName: "pageHeader"
        title: qsTr("Image Editor")
        extraContent.children: [
            IconButton {
                objectName: "aboutButton"
                icon.source: "image://theme/icon-m-about"
                anchors.verticalCenter: parent.verticalCenter
                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
        ]
    }

    SilicaFlickable {
        id: mainFlickable

        objectName: "mainFlickable"
        contentHeight: buttonLayout.height

        anchors {
            top: pageHeader.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        ButtonLayout {
            id: buttonLayout

            objectName: "buttonLayout"

            Button {
                objectName: "createNewImageButton"
                text: qsTr("Create new image")
                onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/CreateEmptyImageDialog.qml"))
            }

            Button {
                objectName: "openImageInEditorButton"
                ButtonLayout.newLine: true
                text: qsTr("Open image in editor")
                onClicked: pageStack.push(imagePickerComponent)
            }
        }
    }

    Component {
        id: imagePickerComponent

        ImagePickerPage {
            onSelectedContentChanged: pageStack.push(Qt.resolvedUrl("../dialogs/ImageEditorDialog.qml"), {
                    "imagePath": selectedContentProperties.filePath,
                    "shouldCreateNewImage": false
                })
        }
    }
}

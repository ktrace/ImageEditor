// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: root

    property var controller: null
    readonly property int penMinimumSize: 3
    readonly property int penMaximumSize: 60

    showNavigationIndicator: false

    Column {
        spacing: 2 * Theme.paddingLarge

        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: Theme.paddingLarge
        }

        Rectangle {
            id: penSample

            height: controller.penSize
            width: height
            radius: height / 2
            anchors.horizontalCenter: parent.horizontalCenter
            color: controller.currentColor
        }

        Item {
            id: penSize

            height: penSizeText.height + penSizeSlider.height + Theme.paddingMedium

            anchors {
                left: parent.left
                right: parent.right
            }

            Text {
                id: penSizeText

                text: qsTr("Pen size")
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeLarge
                horizontalAlignment: Text.AlignHCenter

                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }
            }

            Slider {
                id: penSizeSlider

                onValueChanged: {
                    if (controller.penSize !== value)
                        controller.penSize = value;
                }

                minimumValue: penMinimumSize
                maximumValue: penMaximumSize
                value: controller.penSize

                anchors {
                    top: penSizeText.bottom
                    topMargin: Theme.paddingMedium
                    left: parent.left
                    leftMargin: -2 * Theme.paddingLarge
                    right: parent.right
                    rightMargin: -2 * Theme.paddingLarge
                }
            }
        }

        Item {
            id: colorGrid

            height: gridText.height + gridView.height + Theme.paddingMedium

            anchors {
                left: parent.left
                right: parent.right
            }

            Text {
                id: gridText

                text: qsTr("Pen color")
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeLarge
                horizontalAlignment: Text.AlignHCenter

                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }
            }

            SilicaGridView {
                id: gridView

                height: cellHeight * 3
                anchors {
                    top: gridText.bottom
                    topMargin: Theme.paddingMedium
                    left: parent.left
                    right: parent.right
                }

                cellWidth: width / 4
                cellHeight: cellWidth

                model: controller.colors

                delegate: GridItem {
                    onClicked: controller.currentColor = modelData.color

                    Rectangle {
                        anchors.centerIn: parent
                        height: Theme.itemSizeSmall
                        width: height
                        radius: height / 2
                        color: modelData.color
                        opacity: modelData.checked ? 0.4 : 1
                    }

                    Rectangle {
                        visible: modelData.checked
                        anchors.centerIn: parent
                        width: height
                        radius: height / 2
                        height: Theme.itemSizeSmall
                        color: "transparent"
                        border {
                            color: modelData.color
                            width: 3
                        }
                    }

                    Image {
                        visible: modelData.checked
                        anchors.centerIn: parent
                        source: "image://theme/icon-m-accept"
                    }
                }
            }
        }

        IconButton {
            id: closeButton

            onClicked: {
                controller.submitPenSizeAndColor();
                pageStack.pop();
            }

            anchors.horizontalCenter: parent.horizontalCenter
            icon.source: "image://theme/icon-m-cancel?" + (pressed ? Theme.highlightColor : Theme.primaryColor)
        }
    }
}
